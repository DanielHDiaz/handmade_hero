#include <windows.h>
#include <stdint.h>

#define internal static
#define local_persist static
#define global_variable static

typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

global_variable bool running = true;

global_variable BITMAPINFO bitmapInfo;
global_variable void* bitmapMemory;
global_variable int bitmapWidth;
global_variable int bitmapHeight;
global_variable int bytesPerPixel = 4;

internal void RenderWeirdGradient(int xOffset, int yOffset)
{
    int width = bitmapWidth;
    int height = bitmapHeight;
    int pitch = width * bytesPerPixel;
    uint8* row = (uint8*)bitmapMemory;

    for(int y = 0; y < bitmapHeight; ++y)
    {
        uint32* pixel = (uint32*)row;
        for(int x = 0; x < bitmapWidth; ++x)
        {
            uint8 blue = (x + xOffset);
            uint8 green = (y + yOffset);

            *pixel++ = ((green << 8) | blue);
        }

        row += pitch;
    }
}
internal void ResizeDibSection(int width, int height)
{
    // TODO: Bulletproof this.
    // Maybe don't free first, free after, then free first if that fails.
    
    if(bitmapMemory)
    {
        VirtualFree(bitmapMemory, 0, MEM_RELEASE);
    }

    bitmapWidth = width;
    bitmapHeight = height;

    bitmapInfo.bmiHeader.biSize = sizeof(bitmapInfo.bmiHeader);
    bitmapInfo.bmiHeader.biWidth = bitmapWidth;
    bitmapInfo.bmiHeader.biHeight = -bitmapHeight;
    bitmapInfo.bmiHeader.biPlanes = 1;
    bitmapInfo.bmiHeader.biBitCount = 32;
    bitmapInfo.bmiHeader.biCompression = BI_RGB;

    // Allocate memory myself
    int bitmapMemorySize = (bitmapWidth * bitmapHeight) * bytesPerPixel;
    bitmapMemory = VirtualAlloc(0, bitmapMemorySize, MEM_COMMIT, PAGE_READWRITE);

    // TODO: Clear back buffer to black
}

internal void Win32UpdateWindow(HDC deviceContext, RECT* clientRect, int x, int y, int width, int height)
{
    int windowWidth = clientRect->right - clientRect->left;
    int windowHeight = clientRect->bottom - clientRect->top;

    StretchDIBits(deviceContext, 
                /*
                  x, y, width, height,
                  x, y, width, height,
                */
                  0, 0, bitmapWidth, bitmapHeight,
                  0, 0, windowWidth, windowHeight,
                  bitmapMemory, &bitmapInfo,
                  DIB_RGB_COLORS, SRCCOPY);

}

LRESULT CALLBACK MainWindowCallback(HWND window, UINT message, WPARAM wParam, LPARAM lParam)
{
    LRESULT result = 0;

    switch(message)
    {
        case WM_SIZE:
        {
            RECT clientRect;
            GetWindowRect(window, &clientRect);
            int width = clientRect.right - clientRect.left;
            int height = clientRect.bottom - clientRect.top;

            ResizeDibSection(width, height);
            break;
        }

        case WM_DESTROY:
        {
            running = false;
            break;
        }
        
        case WM_CLOSE:
        {
            running = false;
            break;
        }

        case WM_ACTIVATEAPP:
        {
            break;
        }

        case WM_PAINT:
        {
            PAINTSTRUCT paint;
            HDC deviceContext = BeginPaint(window, &paint);
            int x = paint.rcPaint.left;
            int y = paint.rcPaint.top;
            int width = paint.rcPaint.right - paint.rcPaint.left;
            int height = paint.rcPaint.bottom - paint.rcPaint.top;

            //Win32UpdateWindow(deviceContext, x, y, width, height);

            RECT clientRect;
            GetClientRect(window, &clientRect);
            Win32UpdateWindow(deviceContext, &clientRect, x, y, width, height);
            EndPaint(window, &paint);
            break;
        }

        default:
        {
            // Let the window handle the proccess.
            result = DefWindowProc(window, message, wParam, lParam);
            break;
        }
    }

    return result;
}

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    WNDCLASS windowClass = {};

    // TODO: Check if HREDRAW AND VREDRAW still matter.
    windowClass.style = CS_OWNDC|CS_HREDRAW|CS_VREDRAW;
    windowClass.lpfnWndProc = MainWindowCallback;
    windowClass.hInstance = hInstance;
    //windowClass.hIcon;
    windowClass.lpszClassName = "HandmadeHeroWindowClass";

    if(RegisterClass(&windowClass))
    {
        HWND window = CreateWindowExA(0, windowClass.lpszClassName, "Handmade Hero", WS_OVERLAPPEDWINDOW|WS_VISIBLE,
                                      CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
                                      0, 0, hInstance, 0);

        // Window loop
        if(window)
        {
            int xOffset = 0;
            int yOffset = 0;
            while(running)
            {
                MSG message;
                while(PeekMessage(&message, 0, 0, 0, PM_REMOVE))
                {
                    if(message.message == WM_QUIT)
                    {
                        running = false;
                    }

                    TranslateMessage(&message);
                    DispatchMessageA(&message);
                }

                RenderWeirdGradient(xOffset, yOffset);

                HDC deviceContext = GetDC(window);
                RECT clientRect;
                GetClientRect(window, &clientRect);
                int windowWidth = clientRect.right - clientRect.left;
                int windowHeight = clientRect.bottom - clientRect.top;
                Win32UpdateWindow(deviceContext, &clientRect, 0, 0, windowWidth, windowHeight);
                ReleaseDC(window, deviceContext);

                ++xOffset;
            }
        }

        // The window handle failed to be created.
        else
        {
            // TODO: logging
        }
    }

    // Window class failed to initialize.
    else
    {
        // TODO: logging
    }

    return(0);
}
